package com.android.opisapp.activity;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.opisapp.AppController;
import com.android.opisapp.R;
import com.android.opisapp.adapters.DrawerMenuItemAdapter;
import com.android.opisapp.model.DrawerMenuItem;
import com.android.opisapp.model.UserModel;
import com.android.opisapp.utilities.Constants;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DashboardActivity extends ActionBarActivity implements AdapterView.OnItemClickListener, View.OnClickListener {

    private static UserModel userModel = new UserModel();
    private static ProgressDialog mProgressDialog;
    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ListView mLvDrawerMenu;
    private DrawerMenuItemAdapter mDrawerMenuAdapter;
    private RelativeLayout drawer;
    private Handler mHandler;
    private Intent i;
    private Button btnAppointment, btnDoctor, btnMap, btnHistory, btnHealthTips, btnAbout;
    private String responseResult, responseBody, doctorsStatus;
    private Switch statusSwitch;
    private TextView txtIndicator;

    public static void showDialog(Activity s) {
        mProgressDialog = new ProgressDialog(s);
        // Set progressdialog title
        mProgressDialog.setTitle("Loading");
        // Set progressdialog message
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setIndeterminate(false);
        // Show progressdialog
        mProgressDialog.show();
    }

    public static void hideDialog() {
        // Close the progressdialog
        if (mProgressDialog != null)
            mProgressDialog.dismiss();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        initVars();

        if (savedInstanceState == null) {
            //setFragment(page, Fragment_Home.class);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAppointment:
                i = new Intent(this, Main_Activity.class);
                Main_Activity.page = 0;
                startActivity(i);
                break;

            case R.id.btnDoctor:
                i = new Intent(this, Main_Activity.class);
                Main_Activity.page = 2;
                startActivity(i);
                break;

            case R.id.btnMap:
                i = new Intent(this, Main_Activity.class);
                Main_Activity.page = 3;
                startActivity(i);
                break;

            case R.id.btnHistory:
                i = new Intent(this, Main_Activity.class);
                Main_Activity.page = 4;
                startActivity(i);
                break;

            case R.id.btnHealthTips:
                i = new Intent(this, Main_Activity.class);
                Main_Activity.page = 5;
                startActivity(i);
                break;

            case R.id.btnAbout:
                i = new Intent(this, Main_Activity.class);
                Main_Activity.page = 6;
                startActivity(i);
                break;
        }

    }

    private void initVars() {

        // Fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Set up customize toolbar
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //Referencing widgets
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mLvDrawerMenu = (ListView) findViewById(R.id.lv_drawer_menu);
        drawer = (RelativeLayout) findViewById(R.id.drawer);

        //Set up Listview Items
        List<DrawerMenuItem> menuItems = generateDrawerMenuItems();
        mDrawerMenuAdapter = new DrawerMenuItemAdapter(getApplicationContext(), menuItems);
        mLvDrawerMenu.setAdapter(mDrawerMenuAdapter);
        mLvDrawerMenu.setOnItemClickListener(this);

        //Set up buttons
        btnAppointment = (Button) findViewById(R.id.btnAppointment);
        btnAppointment.setOnClickListener(this);
        btnDoctor = (Button) findViewById(R.id.btnDoctor);
        btnDoctor.setOnClickListener(this);
        btnMap = (Button) findViewById(R.id.btnMap);
        btnMap.setOnClickListener(this);
        btnHistory = (Button) findViewById(R.id.btnHistory);
        btnHistory.setOnClickListener(this);
        btnHealthTips = (Button) findViewById(R.id.btnHealthTips);
        btnHealthTips.setOnClickListener(this);
        btnAbout = (Button) findViewById(R.id.btnAbout);
        btnAbout.setOnClickListener(this);

        txtIndicator = (TextView) findViewById(R.id.txtIndicator);

        // Switch
        statusSwitch = (Switch) findViewById(R.id.mySwitch);
        statusSwitch.setClickable(false);

        //Set timer for delay
        mHandler = new Handler();

        //Set up drawer animation and actions
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.app_name, R.string.app_name) {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(drawerView.getWindowToken(), 0);
                invalidateOptionsMenu();
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        String url = Constants.baseURL + Constants.doctorStatus;
        getStatus(url);
        showDialog(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        switch (position) {
            case 0:

                break;
            case 1:

                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        i = new Intent(DashboardActivity.this, Main_Activity.class);
                        Main_Activity.page = 7;
                        startActivity(i);
                    }
                }, 300);
                break;

            case 2:
                Log.i("token", userModel.getToken());
                String url = Constants.baseURL + Constants.logoutURL;
                postRequest(url);
                showDialog(this);
                break;

            default:

                break;
        }

        mDrawerLayout.closeDrawer(drawer);
        mLvDrawerMenu.invalidateViews();


    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(drawer)) {
            mDrawerLayout.closeDrawer(drawer);
        } else {
            //super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        return true;
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
// Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    public void setFragment(int position, Class<? extends Fragment> fragmentClass) {
        try {
            Fragment fragment = fragmentClass.newInstance();
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame_container, fragment, fragmentClass.getSimpleName());
            fragmentTransaction.commit();

            mLvDrawerMenu.setItemChecked(position, true);
            mDrawerLayout.closeDrawer(mLvDrawerMenu);
            mLvDrawerMenu.invalidateViews();
        } catch (Exception ex) {
            Log.e("setFragment", ex.getMessage());
        }

    }

    private List<DrawerMenuItem> generateDrawerMenuItems() {
        String[] itemsText = getResources().getStringArray(R.array.nav_drawer_items);
        TypedArray itemsIcon = getResources().obtainTypedArray(R.array.nav_drawer_icons);
        List<DrawerMenuItem> result = new ArrayList<DrawerMenuItem>();
        for (int i = 0; i < itemsText.length; i++) {
            DrawerMenuItem item = new DrawerMenuItem();
            item.setText(itemsText[i]);
            item.setIcon(itemsIcon.getResourceId(i, -1));
            result.add(item);
        }
        return result;
    }

    private void postRequest(String url) {

        // Tag used to cancel the request
        String tag_json_obj = "json_obj_req";

        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.DELETE,
                url, (String) null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("JSON-Response", response.toString());

                        try {

                            responseBody = response.toString();
                            if (responseBody.equals("{\"results\":\"Logout\"}")) {
                                responseResult = response.getString("results");
                                Toast.makeText(DashboardActivity.this, responseResult, Toast.LENGTH_SHORT).show();

                                mHandler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        hideDialog();
                                        i = new Intent(getApplicationContext(), LoginActivity.class);
                                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(i);
                                        finish();
                                    }
                                }, 300);
                            } else {
                                responseResult = response.getString("error");
                                Toast.makeText(DashboardActivity.this, responseResult, Toast.LENGTH_SHORT).show();
                                hideDialog();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error-Volley", "Error: " + error);
                Toast.makeText(DashboardActivity.this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Accept", "application/json");
                headers.put("token", userModel.getToken());
                return headers;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }

    private void getStatus(String url) {

        // Tag used to cancel the request
        String tag_json_obj = "json_obj_req";

        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, (String) null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("JSON-Response", response.toString());

                        try {

                            responseBody = response.toString();
                            responseResult = response.getString("results");
                            Log.i("RESULT", responseResult);

                            if (responseResult.equals("Offline")) {
                                Toast.makeText(DashboardActivity.this, "Doctor is currently Offline", Toast.LENGTH_SHORT).show();
                                statusSwitch.setChecked(false);
                                txtIndicator.setText("Red indicates doctor is currently offline");
                            } else if (responseResult.equals("Online")) {
                                Toast.makeText(DashboardActivity.this, "Doctor is now Online", Toast.LENGTH_SHORT).show();
                                statusSwitch.setChecked(true);
                                txtIndicator.setText("Green indicates doctor is now online");
                            }

                            hideDialog();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error-Volley", "Error: " + error);
                Toast.makeText(DashboardActivity.this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                hideDialog();
                reloadDialog();

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Accept", "application/json");
                headers.put("token", userModel.getToken());
                return headers;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }

    protected void reloadDialog() {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage("Doctor status can\'t load the data. Reload this page?");
        builder1.setCancelable(false);
        builder1.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        String url = Constants.baseURL + Constants.doctorStatus;
                        getStatus(url);
                        showDialog(DashboardActivity.this);
                    }
                });
        builder1.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


}