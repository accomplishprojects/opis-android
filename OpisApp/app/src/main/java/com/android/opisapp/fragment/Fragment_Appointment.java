package com.android.opisapp.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.opisapp.AppController;
import com.android.opisapp.R;
import com.android.opisapp.model.UserModel;
import com.android.opisapp.utilities.Constants;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by luigigo on 8/31/15.
 */
public class Fragment_Appointment extends Fragment implements View.OnClickListener {

    static EditText inputId;
    private static UserModel userModel = new UserModel();
    private static ProgressDialog mProgressDialog;
    private Button btnSubmitAppointment, btnCancelAppointment, btnSelectDate, btnReschedAppointment;
    private SimpleDateFormat dateFormatter;
    private SimpleDateFormat timeFormatter;
    private SlideDateTimeListener listener;
    private TextView txtDate, txtTime;
    private String setDate, setTime;
    private HashMap<String, String> params;
    private String responseBody, responseResult;
    private Spinner spinHour, spinMinutes;

    public static void showDialog(Activity s) {
        mProgressDialog = new ProgressDialog(s);
        // Set progressdialog title
        mProgressDialog.setTitle("Loading");
        // Set progressdialog message
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setIndeterminate(false);
        // Show progressdialog
        mProgressDialog.show();
    }

    public static void hideDialog() {
        // Close the progressdialog
        if (mProgressDialog != null)
            mProgressDialog.dismiss();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_appointment, container, false);

        setUpDateTimeDialog();

        txtDate = (TextView) view.findViewById(R.id.txtDate);
        txtTime = (TextView) view.findViewById(R.id.txtTime);
        spinHour = (Spinner) view.findViewById(R.id.listHours);
        spinMinutes = (Spinner) view.findViewById(R.id.listMinutes);

        btnSubmitAppointment = (Button) view.findViewById(R.id.btnSubmitAppointment);
        btnSubmitAppointment.setOnClickListener(this);
        btnReschedAppointment = (Button) view.findViewById(R.id.btnReschedAppointment);
        btnReschedAppointment.setOnClickListener(this);
        btnCancelAppointment = (Button) view.findViewById(R.id.btnCancelAppointment);
        btnCancelAppointment.setOnClickListener(this);
        btnSelectDate = (Button) view.findViewById(R.id.btnSelectDate);
        btnSelectDate.setOnClickListener(this);
        return view;
    }

    private void setUpDateTimeDialog() {

        dateFormatter = new SimpleDateFormat("yyyy/MM/dd");
        timeFormatter = new SimpleDateFormat("hh:mm");

        listener = new SlideDateTimeListener() {
            @Override
            public void onDateTimeSet(Date date) {
                Toast.makeText(getActivity(), "DATE: " + dateFormatter.format(date), Toast.LENGTH_SHORT).show();
                setDate = dateFormatter.format(date);
                setTime = spinHour.getSelectedItem().toString() + ":" + spinMinutes.getSelectedItem().toString();
                Log.i("Hours", setTime);

                txtDate.setText("Appointment date: " + setDate);
            }

            // Optional cancel listener
            @Override
            public void onDateTimeCancel() {
                /*Toast.makeText(getActivity(),
                        "Canceled", Toast.LENGTH_SHORT).show();*/
            }
        };

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btnSubmitAppointment:

                if (txtDate.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Select date and time first.", Toast.LENGTH_SHORT).show();
                } else {
                    params = new HashMap<String, String>();
                    params.put("appointment_date", setDate);
                    params.put("appointment_time", setTime);
                    params.put("service", "check-up");
                    // Toast.makeText(getActivity(), txtDate.getText().toString(), Toast.LENGTH_SHORT).show();
                    String url = Constants.baseURL + Constants.appointmentURL;
                    postRequest(url);
                    showDialog(getActivity());
                }
                break;

            case R.id.btnReschedAppointment:
                reschedDialog();
                break;

            case R.id.btnCancelAppointment:
                showInputDialog();
                break;

            case R.id.btnSelectDate:
                new SlideDateTimePicker.Builder(getActivity().getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        .setMinDate(new Date())
                                //.setMaxDate(maxDate)
                                //.setIs24HourTime(true)
                                //.setTheme(SlideDateTimePicker.HOLO_DARK)
                                //.setIndicatorColor(Color.parseColor("#990000"))
                        .build()
                        .show();
                break;
        }
    }

    private void postRequest(String url) {

        // Tag used to cancel the request
        String tag_json_obj = "json_obj_req";

        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("JSON-Response", response.toString());
                        try {
                            responseBody = response.toString();
                            if (responseBody.equals("{\"results\":\"Appointment successfully added\"}")) {
                                responseResult = response.getString("results");
                                Toast.makeText(getActivity(), responseResult, Toast.LENGTH_SHORT).show();
                                hideDialog();

                            } else {
                                responseResult = response.getString("error");
                                Toast.makeText(getActivity(), responseResult, Toast.LENGTH_SHORT).show();
                                hideDialog();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error-Volley", "Error: " + error);
                Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Accept", "application/json");
                headers.put("token", userModel.getToken());
                return headers;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }

    protected void showInputDialog() {

        // get prompts.xml view
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.dialog_cancel_appointment, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptView);

        inputId = (EditText) promptView.findViewById(R.id.edtAppointmentId);
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setTitle("CANCEL APPOINTMENT")
                .setIcon(R.drawable.bg_cancel_appointment)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (inputId.getText().toString().equals(null) || inputId.getText().toString().equals("")) {
                            Toast.makeText(getActivity(), "Nothing to cancel", Toast.LENGTH_SHORT).show();
                        } else {
                            //Toast.makeText(getActivity(), inputId.getText().toString(), Toast.LENGTH_SHORT).show();
                            params = new HashMap<String, String>();
                            params.put("id", inputId.getText().toString());
                            String url = Constants.baseURL + Constants.cancelAppointment;
                            cancelRequest(url);
                            showDialog(getActivity());
                        }

                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    protected void reschedDialog() {

        // get prompts.xml view
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.dialog_reschedule, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptView);

        txtDate = (TextView) promptView.findViewById(R.id.txtDate);
        inputId = (EditText) promptView.findViewById(R.id.edtAppointmentId);
        btnSelectDate = (Button) promptView.findViewById(R.id.btnSelectDate);
        btnSelectDate.setOnClickListener(this);
        spinHour = (Spinner) promptView.findViewById(R.id.listHours);
        spinMinutes = (Spinner) promptView.findViewById(R.id.listMinutes);

        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setTitle("RESCHEDULE APPOINTMENT")
                .setIcon(R.drawable.ic_resched)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (inputId.getText().toString().equals(null) || inputId.getText().toString().equals("")) {
                            Toast.makeText(getActivity(), "Please enter appointment ID", Toast.LENGTH_SHORT).show();
                        } else if (txtDate.getText().toString().equals(null) || txtDate.getText().toString().equals("")) {
                            Toast.makeText(getActivity(), "Please select date and time first", Toast.LENGTH_SHORT).show();
                        } else {
                            //Toast.makeText(getActivity(), inputId.getText().toString(), Toast.LENGTH_SHORT).show();
                            params = new HashMap<String, String>();
                            params.put("id", inputId.getText().toString());
                            params.put("appointment_date", setDate);
                            params.put("appointment_time", setTime);
                            Log.i("deyt", setTime + ", " + setDate);
                            String url = Constants.baseURL + Constants.reschedAppointment;
                            reschedRequest(url);
                            showDialog(getActivity());

                        }

                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private void cancelRequest(String url) {

        // Tag used to cancel the request
        String tag_json_obj = "json_obj_req";

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("JSON-Response", response.toString());

                        try {
                            responseBody = response.toString();
                            if (responseBody.equals("{\"error\":\"No Appointment\"}")) {
                                responseResult = response.getString("error");
                                Toast.makeText(getActivity(), responseResult, Toast.LENGTH_SHORT).show();
                                hideDialog();
                                txtDate.setText(null);
                            } else {
                                responseResult = response.getString("results");
                                Toast.makeText(getActivity(), responseResult, Toast.LENGTH_SHORT).show();
                                hideDialog();
                                txtDate.setText(null);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // txtDate.setText(null);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error-Volley", "Error: " + error);
                Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("token", userModel.getToken());
                return headers;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }

    private void reschedRequest(String url) {

        // Tag used to cancel the request
        String tag_json_obj = "json_obj_req";

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("JSON-Response", response.toString());

                        try {
                            responseBody = response.toString();
                            if (responseBody.equals("{\"error\":\"Your schedule is not available. Try another schedule.\"}")) {
                                responseResult = response.getString("error");
                                Toast.makeText(getActivity(), responseResult, Toast.LENGTH_SHORT).show();
                                hideDialog();
                                txtDate.setText(null);
                            } else if (responseBody.equals("{\"error\":\"Invalid user\"}")) {
                                responseResult = response.getString("error");
                                Toast.makeText(getActivity(), responseResult, Toast.LENGTH_SHORT).show();
                                hideDialog();
                                txtDate.setText(null);
                            } else {
                                responseResult = response.getString("results");
                                Toast.makeText(getActivity(), responseResult, Toast.LENGTH_SHORT).show();
                                hideDialog();
                                txtDate.setText(null);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // txtDate.setText(null);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error-Volley", "Error: " + error);
                Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("token", userModel.getToken());
                return headers;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }


}
