package com.android.opisapp.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.opisapp.AppController;
import com.android.opisapp.R;
import com.android.opisapp.activity.LoginActivity;
import com.android.opisapp.utilities.Constants;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by luigigo on 8/30/15.
 */
public class Fragment_NewAccount extends Fragment implements View.OnClickListener {

    private static ProgressDialog mProgressDialog;
    private Button btnCreateAccount, btnCancel, btnBirthdate;
    private Intent i;
    private EditText edtPatientNo, edtUsername, edtPassword, edtFirstname, edtLastname,
            edtMiddlename, edtEmailAdd, edtContact;
    private String patientNo, username, password, firstname, lastname, middlename, birthday,
            emailAdd, contact, civilStatus;
    private String responseBody, responseResult;
    private HashMap<String, String> params;
    private Spinner listCivilStatus;
    private SimpleDateFormat dateFormatter;
    private SlideDateTimeListener listener;
    private String setDate;
    private Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
            + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
            + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
            + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
            + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
            + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$");

    public static void showDialog(Activity s) {
        mProgressDialog = new ProgressDialog(s);
        // Set progressdialog title
        mProgressDialog.setTitle("Loading");
        // Set progressdialog message
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setIndeterminate(false);
        // Show progressdialog
        mProgressDialog.show();
    }

    public static void hideDialog() {
        // Close the progressdialog
        if (mProgressDialog != null)
            mProgressDialog.dismiss();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_newaccount, container, false);

        btnCreateAccount = (Button) view.findViewById(R.id.btnCreateAccount);
        btnCancel = (Button) view.findViewById(R.id.btnCancel);

        edtUsername = (EditText) view.findViewById(R.id.edtUsername);
        edtPassword = (EditText) view.findViewById(R.id.edtPassword);
        edtFirstname = (EditText) view.findViewById(R.id.edtFirstname);
        edtLastname = (EditText) view.findViewById(R.id.edtLastname);
        edtMiddlename = (EditText) view.findViewById(R.id.edtMiddlename);
        btnBirthdate = (Button) view.findViewById(R.id.btnBirthdate);
        btnBirthdate.setOnClickListener(this);
        edtEmailAdd = (EditText) view.findViewById(R.id.edtEmailAdd);
        edtContact = (EditText) view.findViewById(R.id.edtContactNo);
        listCivilStatus = (Spinner) view.findViewById(R.id.listCivilStatus);

        btnCreateAccount.setOnClickListener(this);
        btnCancel.setOnClickListener(this);

        setUpDateTimeDialog();
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCreateAccount:
                username = edtUsername.getText().toString();
                password = edtPassword.getText().toString();
                firstname = edtFirstname.getText().toString();
                lastname = edtLastname.getText().toString();
                middlename = edtMiddlename.getText().toString();
                birthday = btnBirthdate.getText().toString();
                emailAdd = edtEmailAdd.getText().toString();
                contact = edtContact.getText().toString();
                civilStatus = listCivilStatus.getSelectedItem().toString();

                if (!validation()) {
                    Toast.makeText(getActivity(), "Please check all your inputs", Toast.LENGTH_SHORT).show();
                } else if (contact.length() < 11 || !contact.matches("^[0-9]*$")) {
                    Toast.makeText(getActivity(), "Invalid contact number", Toast.LENGTH_SHORT).show();
                } else if (!checkEmail(emailAdd)) {
                    Toast.makeText(getActivity(), "Invalid Email Address", Toast.LENGTH_SHORT).show();
                } else {
                    params = new HashMap<String, String>();
                    params.put("username", username);
                    params.put("password", password);
                    params.put("firstname", firstname);
                    params.put("lastname", lastname);
                    params.put("middlename", middlename);
                    params.put("bday", birthday);
                    params.put("email", emailAdd);
                    params.put("contactnum", contact);
                    params.put("civilstatus", civilStatus);
                    String url = Constants.baseURL + Constants.registrationURL;
                    postRequest(url);
                    showDialog(getActivity());
                }
                break;
            case R.id.btnCancel:
                i = new Intent(getActivity(), LoginActivity.class);
                startActivity(i);
                getActivity().finish();

                /*Fragment fragment = new Fragment_Obgyne();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment);
                fragmentTransaction.commit();*/
                break;
            case R.id.btnBirthdate:
                new SlideDateTimePicker.Builder(getActivity().getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                                //.setMinDate(new Date())
                        .setMaxDate(new Date())
                                //.setIs24HourTime(true)
                                //.setTheme(SlideDateTimePicker.HOLO_DARK)
                                //.setIndicatorColor(Color.parseColor("#990000"))
                        .build()
                        .show();
                break;

        }

    }

    private boolean checkEmail(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }


    private Boolean validation() {

        if (username.isEmpty())
            return false;
        else if (password.isEmpty())
            return false;
        else if (firstname.isEmpty())
            return false;
        else if (lastname.isEmpty())
            return false;
        else if (middlename.isEmpty())
            return false;
        else if (birthday.isEmpty())
            return false;
        else if (emailAdd.isEmpty())
            return false;
        else if (contact.isEmpty())
            return false;
        else if (civilStatus.isEmpty())
            return false;
        else
            return true;
    }

    private void postRequest(String url) {

        // Tag used to cancel the request
        String tag_json_obj = "json_obj_req";

        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("JSON-Response", response.toString());

                        try {
                            responseBody = response.toString();

                            if (responseBody.equals("{\"error\":\"Username is already taken\"}")) {
                                responseResult = response.getString("error");
                                Toast.makeText(getActivity(), responseResult, Toast.LENGTH_SHORT).show();
                                hideDialog();
                            } else if (responseBody.equals("{\"results\":\"Patient successfully registered\"}")) {
                                responseResult = response.getString("results");
                                Toast.makeText(getActivity(), responseResult, Toast.LENGTH_SHORT).show();
                                hideDialog();

                                i = new Intent(getActivity(), LoginActivity.class);
                                startActivity(i);
                                getActivity().finish();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error-Volley", "Error: " + error);
                Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                hideDialog();
               /* try {
                    if (error.networkResponse.data != null) {
                        responseResult = null;
                        String responseBody = new String(error.networkResponse.data, "UTF-8");
                        jsonObject = new JSONObject(responseBody);
                        responseResult = jsonObject.getString("error");
                        Log.i("Error-Login", responseResult);
                    }

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }*/
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Accept", "application/json");
                return headers;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }

    private void setUpDateTimeDialog() {

        dateFormatter = new SimpleDateFormat("yyyy/MM/dd");

        listener = new SlideDateTimeListener() {
            @Override
            public void onDateTimeSet(Date date) {
                Toast.makeText(getActivity(), "DATE: " + dateFormatter.format(date), Toast.LENGTH_SHORT).show();
                setDate = dateFormatter.format(date);
                btnBirthdate.setText(setDate);
            }

            // Optional cancel listener
            @Override
            public void onDateTimeCancel() {
                /*Toast.makeText(getActivity(),
                        "Canceled", Toast.LENGTH_SHORT).show();*/
            }
        };

    }

}
