package com.android.opisapp.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.opisapp.AppController;
import com.android.opisapp.R;
import com.android.opisapp.model.UserModel;
import com.android.opisapp.utilities.Constants;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by luigigo on 9/5/15.
 */
public class Fragment_Contact_Doctor extends Fragment implements View.OnClickListener {

    static ProgressDialog mProgressDialog;
    private static UserModel userModel = new UserModel();
    View view;
    String responseBody, responseResult;
    private Button btnMakeCall, btnSendSMS, btnSendMessage;
    private AlertDialog alert;
    private HashMap<String, String> params;

    public static void showDialog(Activity s) {
        mProgressDialog = new ProgressDialog(s);
        // Set progressdialog title
        mProgressDialog.setTitle("Loading");
        // Set progressdialog message
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setIndeterminate(false);
        // Show progressdialog
        mProgressDialog.show();
    }

    public static void hideDialog() {
        // Close the progressdialog
        if (mProgressDialog != null)
            mProgressDialog.dismiss();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_contact_doctor, container, false);

        btnMakeCall = (Button) view.findViewById(R.id.btnMakeCall);
        btnMakeCall.setOnClickListener(this);
        btnSendSMS = (Button) view.findViewById(R.id.btnSendSMS);
        btnSendSMS.setOnClickListener(this);
        btnSendMessage = (Button) view.findViewById(R.id.btnSendMessage);
        btnSendMessage.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnMakeCall:
                makeCall();
                break;

            case R.id.btnSendSMS:
                // Launch SMS Application
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                intent.setType("vnd.android-dir/mms-sms");
                startActivity(intent);
                break;

            case R.id.btnSendMessage:
                sendMessage();
                break;

        }
    }

    protected void makeCall() {

        // get prompts.xml view
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.dialog_make_call, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptView);

        final EditText number = (EditText) promptView.findViewById(R.id.edtPhoneNumber);
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setTitle("Call your Doctor")
                .setIcon(R.drawable.ic_phone_black)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        String phoneNumber = number.getText().toString().trim();
                        Log.i("PHONE NUMBER", number.getText().toString().trim());
                        if (phoneNumber.isEmpty() || phoneNumber.equals("") || phoneNumber.length() < 11) {
                            Toast.makeText(getActivity(), "Invalid phone number", Toast.LENGTH_SHORT).show();

                        } else if (phoneNumber != null && phoneNumber.matches("^[0-9]*$")) {
                            Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber));
                            startActivity(callIntent);

                        }


                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        alert = alertDialogBuilder.create();
        alert.show();
    }

    protected void sendMessage() {

        // get prompts.xml view
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.dialog_send_sms, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptView);

        final EditText message = (EditText) promptView.findViewById(R.id.edtSmsBody);

        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setTitle("Send a message")
                .setIcon(R.drawable.ic_message_black)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        String messageBody = message.getText().toString().trim();

                        try {
                            if (messageBody.length() == 0 || messageBody.isEmpty()) {
                                Toast.makeText(getActivity(), "Invalid Message", Toast.LENGTH_SHORT).show();
                            } else {
                                params = new HashMap<String, String>();
                                params.put("message", messageBody);
                                String url = Constants.baseURL + Constants.sendMessage;
                                sendMessage(url);
                                showDialog(getActivity());
                            }


                        } catch (Exception e) {
                            Toast.makeText(getActivity(),
                                    "SMS failed, please try again later!",
                                    Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    }


                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        alert = alertDialogBuilder.create();
        alert.show();
    }

    private void sendMessage(String url) {

        // Tag used to cancel the request
        String tag_json_obj = "json_obj_req";

        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("JSON-Response", response.toString());

                        try {
                            responseBody = response.toString();
                            if (responseBody.equals("{\"results\":\"Message successfully sent! \"}")) {
                                responseResult = response.getString("results");
                                hideDialog();
                                Toast.makeText(getActivity(), responseResult, Toast.LENGTH_SHORT).show();
                            } else {
                                responseResult = response.getString("error");
                                hideDialog();
                                Toast.makeText(getActivity(), responseResult, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error-Volley", "Error: " + error);
                Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Accept", "application/json");
                headers.put("token", userModel.getToken());
                return headers;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }

}
