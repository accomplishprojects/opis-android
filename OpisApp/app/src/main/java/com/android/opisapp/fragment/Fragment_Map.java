package com.android.opisapp.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.opisapp.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by luigigo on 9/26/15.
 */
public class Fragment_Map extends Fragment {

    GoogleMap googleMap;
    LocationManager locationManager;
    Location location;
    CameraUpdate cameraUpdate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        try {
            // Loading map
            initilizeMap();
            // getCurrentLocation();
            // Move the camera instantly to location with a zoom of 15.
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(14.769893, 120.9409122), 15));

            // Zoom in, animating the camera.
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
            googleMap.addMarker(new MarkerOptions().position(
                    new LatLng(14.769893, 120.9409122))
                    .title("Sandoval-Victoriano OB/GYN Clinic")
                    .snippet("Manila N Rd, Marilao, Bulacan")
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker)))
                    .showInfoWindow();

            /*locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                Log.i("Loc", (location == null) + "");
                Log.i("LocManager", (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) == true) + "");
                //  location = googleMap.getMyLocation();
                if (location != null) {
                    LatLng latLang = new LatLng(location.getLatitude(), location.getLongitude());
                    cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLang, 10);
                    googleMap.animateCamera(cameraUpdate);
                } else {

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Warning");
                    builder.setMessage("Location Not Found...!!!");
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).show();
                }
            } else {
                showAlertDialog();
            }*/

        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    private void initilizeMap() {
        if (googleMap == null) {
            googleMap = ((MapFragment) getActivity().getFragmentManager().findFragmentById(
                    R.id.map)).getMap();
            googleMap.setMyLocationEnabled(true);

            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getActivity(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    private void showAlertDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Warning");
        builder.setMessage("Device GPS is currently OFF. Please Turn ON.");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).show();

    }

    void getCurrentLocation() {
        Location myLocation = googleMap.getMyLocation();
        if (myLocation != null) {
            double dLatitude = myLocation.getLatitude();
            double dLongitude = myLocation.getLongitude();
            Log.i("APPLICATION", " : " + dLatitude);
            Log.i("APPLICATION", " : " + dLongitude);
            googleMap.addMarker(new MarkerOptions().position(
                    new LatLng(dLatitude, dLongitude)).title("My Location"));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(dLatitude, dLongitude), 8));

        } else {
            Toast.makeText(getActivity(), "Unable to fetch the current location", Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        initilizeMap();
    }
}