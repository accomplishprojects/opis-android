package com.android.opisapp.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.opisapp.AppController;
import com.android.opisapp.R;
import com.android.opisapp.model.UserModel;
import com.android.opisapp.utilities.Constants;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by luigigo on 8/30/15.
 */
public class LoginActivity extends Activity implements View.OnClickListener {

    private static UserModel userModel = new UserModel();
    private Handler mHandler;
    private TextView txtCreateAccount;
    private Button btnLogin;
    private Intent i;
    private HashMap<String, String> params;
    private String responseResult, responseBody;
    private String username, password;
    private EditText edtUsername, edtPassword;
    private static ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initVars();

    }

    private void initVars() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        edtUsername = (EditText) findViewById(R.id.edtUsername);
        edtPassword = (EditText) findViewById(R.id.edtPassword);

        btnLogin = (Button) findViewById(R.id.btnLogin);
        txtCreateAccount = (TextView) findViewById(R.id.txtCreateAccount);

        btnLogin.setOnClickListener(this);
        txtCreateAccount.setOnClickListener(this);

        mHandler = new Handler();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnLogin:

                username = edtUsername.getText().toString();
                password = edtPassword.getText().toString();

                if(username.isEmpty() || password.isEmpty()) {
                    Toast.makeText(LoginActivity.this, "Please check username and password", Toast.LENGTH_SHORT).show();

                }else{
                    params = new HashMap<String, String>();
                    params.put("username", username);
                    params.put("password", password);
                    String url = Constants.baseURL + Constants.loginURL;
                    postRequest(url);
                    showDialog(this);
                }

                break;
            case R.id.txtCreateAccount:
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        i = new Intent(LoginActivity.this, Main_Activity.class);
                        Main_Activity.page = 1;
                        startActivity(i);
                        finish();
                    }
                }, 300);
                break;

        }

    }

    private void postRequest(String url) {

        // Tag used to cancel the request
        String tag_json_obj = "json_obj_req";

        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("JSON-Response", response.toString());

                        try {
                            responseBody = response.toString();
                            if (responseBody.equals("{\"error\":\"Invalid username or password\"}")) {
                                responseResult = response.getString("error");
                                Toast.makeText(LoginActivity.this, responseResult, Toast.LENGTH_SHORT).show();
                                Log.d("Error-Login", responseResult);
                                hideDialog();

                            } else {
                                responseResult = response.getString("results");
                                userModel.setToken(responseResult.substring(6));

                                mHandler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        hideDialog();
                                        i = new Intent(LoginActivity.this, DashboardActivity.class);
                                        startActivity(i);
                                        finish();

                                    }
                                }, 300);
                                Log.d("Success-Login", responseResult.substring(6));

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error-Volley", "Error: " + error);
                hideDialog();
                Toast.makeText(LoginActivity.this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();

               /* try {
                    if (error.networkResponse.data != null) {
                        responseResult = null;
                        String responseBody = new String(error.networkResponse.data, "UTF-8");
                        jsonObject = new JSONObject(responseBody);
                        responseResult = jsonObject.getString("error");
                        Log.i("Error-Login", responseResult);
                    }

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }*/
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Accept", "application/json");
                return headers;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }

    public static void showDialog(Activity s) {
        mProgressDialog = new ProgressDialog(s);
        // Set progressdialog title
        mProgressDialog.setTitle("Loading");
        // Set progressdialog message
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setIndeterminate(false);
        // Show progressdialog
        mProgressDialog.show();
    }


    public static void hideDialog() {
        // Close the progressdialog
        if (mProgressDialog != null)
            mProgressDialog.dismiss();
    }
}
