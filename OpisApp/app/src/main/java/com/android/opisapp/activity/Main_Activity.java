package com.android.opisapp.activity;


import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.opisapp.AppController;
import com.android.opisapp.R;
import com.android.opisapp.adapters.DrawerMenuItemAdapter;
import com.android.opisapp.fragment.Fragment_About;
import com.android.opisapp.fragment.Fragment_Appointment;
import com.android.opisapp.fragment.Fragment_Contact_Doctor;
import com.android.opisapp.fragment.Fragment_HealthTips;
import com.android.opisapp.fragment.Fragment_History;
import com.android.opisapp.fragment.Fragment_Map;
import com.android.opisapp.fragment.Fragment_NewAccount;
import com.android.opisapp.fragment.Fragment_Reset_Password;
import com.android.opisapp.model.DrawerMenuItem;
import com.android.opisapp.model.UserModel;
import com.android.opisapp.utilities.Constants;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Main_Activity extends ActionBarActivity implements AdapterView.OnItemClickListener {

    public static int page;
    private static UserModel userModel = new UserModel();
    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ListView mLvDrawerMenu;
    private DrawerMenuItemAdapter mDrawerMenuAdapter;
    private RelativeLayout drawer;
    private Handler mHandler;
    private Intent i;
    private String responseResult, responseBody;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initVars();

        if (savedInstanceState == null) {
            //setFragment(page, Fragment_Home.class);
        }

        switch (page) {
            case 0:
                setFragment(page, Fragment_Appointment.class);
                break;
            case 1:
                setFragment(page, Fragment_NewAccount.class);
                mDrawerToggle.setDrawerIndicatorEnabled(false);
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

                break;

            case 2:
                setFragment(page, Fragment_Contact_Doctor.class);
                break;

            case 3:
                setFragment(page, Fragment_Map.class);
                break;

            case 4:
                setFragment(page, Fragment_History.class);
                break;
            case 5:
                setFragment(page, Fragment_HealthTips.class);
                break;

            case 6:
                setFragment(page, Fragment_About.class);
                break;
            case 7:
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        setFragment(page, Fragment_Reset_Password.class);
                    }
                }, 300);

                break;
        }
    }

    private void initVars() {

        // Fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Set up customize toolbar
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //Referencing widgets
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mLvDrawerMenu = (ListView) findViewById(R.id.lv_drawer_menu);
        drawer = (RelativeLayout) findViewById(R.id.drawer);

        //Set up Listview Items
        List<DrawerMenuItem> menuItems = generateDrawerMenuItems();
        mDrawerMenuAdapter = new DrawerMenuItemAdapter(getApplicationContext(), menuItems);
        mLvDrawerMenu.setAdapter(mDrawerMenuAdapter);
        mLvDrawerMenu.setOnItemClickListener(this);

        //Set timer for delay
        mHandler = new Handler();

        //Set up drawer animation and actions
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.app_name, R.string.app_name) {

            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(drawerView.getWindowToken(), 0);
                invalidateOptionsMenu();
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        switch (position) {
            case 0:
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        i = new Intent(Main_Activity.this, DashboardActivity.class);
                        startActivity(i);
                        finish();
                    }
                }, 300);
                break;

            case 1:
                setFragment(page, Fragment_Reset_Password.class);
                break;

            case 2:
                String url = Constants.baseURL + Constants.logoutURL;
                postRequest(url);
                break;
            default:

                break;
        }

        mDrawerLayout.closeDrawer(drawer);
        mLvDrawerMenu.invalidateViews();


    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(drawer)) {
            mDrawerLayout.closeDrawer(drawer);
        } else {
            if (page == 1) {

            } else
                super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        return true;
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
// Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    public void setFragment(int position, Class<? extends Fragment> fragmentClass) {
        try {
            Fragment fragment = fragmentClass.newInstance();
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame_container, fragment, fragmentClass.getSimpleName());
            fragmentTransaction.commit();

            mLvDrawerMenu.setItemChecked(position, true);
            mDrawerLayout.closeDrawer(mLvDrawerMenu);
            mLvDrawerMenu.invalidateViews();
        } catch (Exception ex) {
            Log.e("setFragment", ex.getMessage());
        }

    }

    private List<DrawerMenuItem> generateDrawerMenuItems() {

        String[] itemsText = getResources().getStringArray(R.array.nav_drawer_items);
        TypedArray itemsIcon = getResources().obtainTypedArray(R.array.nav_drawer_icons);
        List<DrawerMenuItem> result = new ArrayList<DrawerMenuItem>();

        for (int i = 0; i < itemsText.length; i++) {

            DrawerMenuItem item = new DrawerMenuItem();
            item.setText(itemsText[i]);
            item.setIcon(itemsIcon.getResourceId(i, -1));
            result.add(item);
        }

        return result;
    }

    private void postRequest(String url) {

        // Tag used to cancel the request
        String tag_json_obj = "json_obj_req";

        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.DELETE,
                url, (String) null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("JSON-Response", response.toString());

                        try {
                            responseBody = response.toString();
                            if (responseBody.equals("{\"results\":\"Logout\"}")) {
                                responseResult = response.getString("results");
                                Toast.makeText(Main_Activity.this, responseResult, Toast.LENGTH_SHORT).show();

                                mHandler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        i = new Intent(getApplicationContext(), LoginActivity.class);
                                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(i);
                                        finish();
                                    }
                                }, 300);
                            } else {
                                responseResult = response.getString("error");
                                Toast.makeText(Main_Activity.this, responseResult, Toast.LENGTH_SHORT).show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error-Volley", "Error: " + error);
                Toast.makeText(Main_Activity.this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Accept", "application/json");
                headers.put("token", userModel.getToken());
                return headers;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }

}