package com.android.opisapp.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.android.opisapp.AppController;
import com.android.opisapp.R;
import com.android.opisapp.adapters.ListViewAdapter;
import com.android.opisapp.model.HistoryModel;
import com.android.opisapp.model.UserModel;
import com.android.opisapp.utilities.Constants;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by luigigo on 9/27/15.
 */
public class Fragment_History extends Fragment implements View.OnClickListener {

    private static UserModel userModel = new UserModel();
    private static ProgressDialog mProgressDialog;
    ListViewAdapter adapter;
    List<HistoryModel> historyList = new ArrayList<HistoryModel>();
    private String responseBody, responseResult;
    private JSONArray jsonArray;
    private JSONObject jsonObj;
    private ListView listView;

    public static void showDialog(Activity s) {
        mProgressDialog = new ProgressDialog(s);
        // Set progressdialog title
        mProgressDialog.setTitle("Loading");
        // Set progressdialog message
        mProgressDialog.setCancelable(true);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setIndeterminate(false);
        // Show progressdialog
        mProgressDialog.show();
    }

    public static void hideDialog() {
        // Close the progressdialog
        if (mProgressDialog != null)
            mProgressDialog.dismiss();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);

        listView = (ListView) view.findViewById(R.id.listview);


        String url = Constants.baseURL + Constants.appointmentHistory;
        postRequest(url);
        showDialog(getActivity());
        return view;
    }

    private void postRequest(String url) {

        // Tag used to cancel the request
        String tag_json_obj = "json_obj_req";

        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, (String) null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("JSON-Response", response.toString());

                        try {
                            responseBody = response.toString();
                            if (responseBody.equals("{\"results\":\"No appointment history\"}")) {
                                responseResult = response.getString("results");
                                Toast.makeText(getActivity(), responseResult, Toast.LENGTH_SHORT).show();
                                hideDialog();

                            } else {
                                jsonArray = response.getJSONArray("results");
                                Log.i("jsonObj", "" + jsonArray);

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject obj = jsonArray.getJSONObject(i);

                                    HistoryModel historyModel = new HistoryModel();
                                    historyModel.setUserId(obj.getString("id"));
                                    historyModel.setPatientNumber(obj.getString("patient_number"));
                                    historyModel.setAppointmentDate(obj.getString("appointment_date"));
                                    historyModel.setAppointmentTime(obj.getString("appointment_time"));
                                    historyModel.setService(obj.getString("service"));
                                    historyList.add(historyModel);

                                    Log.i("Id", "" + historyModel.getUserId());
                                }

                                adapter = new ListViewAdapter(getActivity(), historyList);
                                listView.setAdapter(adapter);
                                hideDialog();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error-Volley", "Error: " + error);
                Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                hideDialog();
                reloadDialog();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Accept", "application/json");
                headers.put("token", userModel.getToken());
                return headers;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }

    protected void reloadDialog() {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage("Can\'t load appointment records. Reload this page?");
        builder1.setCancelable(false);
        builder1.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        String url = Constants.baseURL + Constants.appointmentHistory;
                        postRequest(url);
                        showDialog(getActivity());
                    }
                });
        builder1.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSelectDate:

                break;
        }
    }
}
