package com.android.opisapp.model;

/**
 * Created by luigigo on 9/26/15.
 */
public class UserModel {

    public static String token;
    public static boolean status = false;

    public static boolean isStatus() {
        return status;
    }

    public static void setStatus(boolean status) {
        UserModel.status = status;
    }

    public static String userId, patientNumber, appointmentDate, appointmentTime, service;

    public static String getUserId() {
        return userId;
    }

    public static void setUserId(String userId) {
        UserModel.userId = userId;
    }

    public static String getPatientNumber() {
        return patientNumber;
    }

    public static void setPatientNumber(String patientNumber) {
        UserModel.patientNumber = patientNumber;
    }

    public static String getAppointmentDate() {
        return appointmentDate;
    }

    public static void setAppointmentDate(String appointmentDate) {
        UserModel.appointmentDate = appointmentDate;
    }

    public static String getAppointmentTime() {
        return appointmentTime;
    }

    public static void setAppointmentTime(String appointmentTime) {
        UserModel.appointmentTime = appointmentTime;
    }

    public static String getService() {
        return service;
    }

    public static void setService(String service) {
        UserModel.service = service;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}

