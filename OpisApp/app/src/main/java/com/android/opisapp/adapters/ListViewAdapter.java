package com.android.opisapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.opisapp.AppController;
import com.android.opisapp.R;
import com.android.opisapp.activity.Main_Activity;
import com.android.opisapp.fragment.Fragment_Appointment;
import com.android.opisapp.model.HistoryModel;
import com.android.opisapp.model.UserModel;
import com.android.opisapp.utilities.Constants;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListViewAdapter extends BaseAdapter implements View.OnClickListener {

    static EditText inputId;
    private static UserModel userModel = new UserModel();
    // Declare Variables
    Context mContext;
    LayoutInflater inflater;
    Fragment_Appointment fm_Appointment;
    private ArrayList<HistoryModel> arraylist;
    private List<HistoryModel> historyList = null;
    private String responseBody, responseResult;
    private HashMap<String, String> params;
    private Spinner spinHour, spinMinutes;
    private Button btnSelectDate;
    private TextView txtDate;
    private String setDate, setTime, setUserId;
    private SimpleDateFormat dateFormatter;
    private SlideDateTimeListener listener;

    public ListViewAdapter(Context context,
                           List<HistoryModel> worldpopulationlist) {
        mContext = context;
        this.historyList = worldpopulationlist;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<HistoryModel>();
        this.arraylist.addAll(historyList);
    }

    @Override
    public int getCount() {
        return historyList.size();
    }

    @Override
    public HistoryModel getItem(int position) {
        return historyList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.listview_item, null);

            // Locate the TextViews in listview_item.xml
            holder.userId = (TextView) view.findViewById(R.id.txtUserId);
            holder.patientNo = (TextView) view.findViewById(R.id.txtPatientNo);
            holder.appointmentDate = (TextView) view.findViewById(R.id.txtDate);
            holder.appointmentTime = (TextView) view.findViewById(R.id.txtTime);
            holder.service = (TextView) view.findViewById(R.id.txtService);
            holder.btnResched = (Button) view.findViewById(R.id.btnResched);
            holder.btnCancel = (Button) view.findViewById(R.id.btnCancel);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        // Set the results into TextViews
        holder.userId.setText(historyList.get(position).getUserId());
        holder.patientNo.setText(historyList.get(position).getPatientNumber());
        holder.appointmentDate.setText(historyList.get(position).getAppointmentDate());
        holder.appointmentTime.setText(historyList.get(position).getAppointmentTime());
        holder.service.setText(historyList.get(position).getService());

        holder.btnResched.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setUserId = historyList.get(position).getUserId().toString();
                setUpDateTimeDialog();
                reschedDialog();

            }
        });

        holder.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String appointmentId = historyList.get(position).getUserId().toString();
                params = new HashMap<String, String>();
                params.put("id", appointmentId);
                String url = Constants.baseURL + Constants.cancelAppointment;
                cancelRequest(url);
            }
        });

        return view;
    }

    protected void reschedDialog() {

        // get prompts.xml view
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View promptView = layoutInflater.inflate(R.layout.dialog_reschedule, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setView(promptView);

        txtDate = (TextView) promptView.findViewById(R.id.txtDate);
        inputId = (EditText) promptView.findViewById(R.id.edtAppointmentId);
        btnSelectDate = (Button) promptView.findViewById(R.id.btnSelectDate);
        btnSelectDate.setOnClickListener(this);
        spinHour = (Spinner) promptView.findViewById(R.id.listHours);
        spinMinutes = (Spinner) promptView.findViewById(R.id.listMinutes);

        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setTitle("RESCHEDULE APPOINTMENT")
                .setIcon(R.drawable.ic_resched)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (inputId.getText().toString().equals(null) || inputId.getText().toString().equals("")) {
                            Toast.makeText(mContext, "Please enter appointment ID", Toast.LENGTH_SHORT).show();
                        } else if (txtDate.getText().toString().equals(null) || txtDate.getText().toString().equals("")) {
                            Toast.makeText(mContext, "Please select date and time first", Toast.LENGTH_SHORT).show();
                        } else {
                            //Toast.makeText(getActivity(), inputId.getText().toString(), Toast.LENGTH_SHORT).show();
                            params = new HashMap<String, String>();
                            params.put("id", setUserId);
                            params.put("appointment_date", setDate);
                            params.put("appointment_time", setTime);
                            Log.i("deyt", setTime + ", " + setDate);
                            String url = Constants.baseURL + Constants.reschedAppointment;
                            reschedRequest(url);
                            //showDialog(getActivity());

                        }

                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private void cancelRequest(String url) {

        // Tag used to cancel the request
        String tag_json_obj = "json_obj_req";

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("JSON-Response", response.toString());

                        try {
                            responseBody = response.toString();
                            if (responseBody.equals("{\"error\":\"No Appointment\"}")) {
                                responseResult = response.getString("error");
                                Toast.makeText(mContext, responseResult, Toast.LENGTH_SHORT).show();
                            } else {
                                responseResult = response.getString("results");
                                Toast.makeText(mContext, responseResult, Toast.LENGTH_SHORT).show();

                            }

                            Intent i = new Intent(mContext, Main_Activity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
                            Main_Activity.page = 4;
                            mContext.startActivity(i);
                            ((Activity) mContext).finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // txtDate.setText(null);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error-Volley", "Error: " + error);
                Toast.makeText(mContext, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("token", userModel.getToken());
                return headers;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }

    private void reschedRequest(String url) {

        // Tag used to cancel the request
        String tag_json_obj = "json_obj_req";

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("JSON-Response", response.toString());

                        try {
                            responseBody = response.toString();
                            if (responseBody.equals("{\"error\":\"Your schedule is not available. Try another schedule.\"}")) {
                                responseResult = response.getString("error");
                                Toast.makeText(mContext, responseResult, Toast.LENGTH_SHORT).show();

                                txtDate.setText(null);
                            } else {
                                responseResult = response.getString("results");
                                Toast.makeText(mContext, responseResult, Toast.LENGTH_SHORT).show();

                                txtDate.setText(null);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // txtDate.setText(null);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error-Volley", "Error: " + error);
                Toast.makeText(mContext, "Please check your internet connection.", Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("token", userModel.getToken());
                return headers;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSelectDate:

            /*    Toast.makeText(mContext, "Hahahah Gumana!", Toast.LENGTH_SHORT).show();
                fm_Appointment = new Fragment_Appointment();

                new SlideDateTimePicker.Builder(fm_Appointment.getActivity().getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        .setMinDate(new Date())
                                //.setMaxDate(maxDate)
                                //.setIs24HourTime(true)
                                //.setTheme(SlideDateTimePicker.HOLO_DARK)
                                //.setIndicatorColor(Color.parseColor("#990000"))
                        .build()
                        .show();*/
                break;
        }
    }

    private void setUpDateTimeDialog() {

        dateFormatter = new SimpleDateFormat("yyyy/MM/dd");

        listener = new SlideDateTimeListener() {
            @Override
            public void onDateTimeSet(Date date) {
                Toast.makeText(mContext, "DATE: " + dateFormatter.format(date), Toast.LENGTH_SHORT).show();
                setDate = dateFormatter.format(date);
                setTime = spinHour.getSelectedItem().toString() + ":" + spinMinutes.getSelectedItem().toString();
                Log.i("Hours", setTime);

                txtDate.setText("Appointment date: " + setDate);
            }

            // Optional cancel listener
            @Override
            public void onDateTimeCancel() {
                /*Toast.makeText(getActivity(),
                        "Canceled", Toast.LENGTH_SHORT).show();*/
            }
        };

    }

    public class ViewHolder {
        TextView userId, patientNo, appointmentDate, appointmentTime, service;
        Button btnResched, btnCancel;

    }
}