package com.android.opisapp.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.android.opisapp.R;

/**
 * Created by luigigo on 8/21/15.
 */
public class Fragment_Home extends Fragment {

    public static Button btnObgyne;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);

        btnObgyne = (Button)view.findViewById(R.id.btnAppointment);

        return view;
    }


}
