package com.android.opisapp.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.opisapp.R;

/**
 * Created by luigigo on 9/28/15.
 */
public class Fragment_HealthTips extends Fragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_healthtips, container, false);

        return view;
    }

}
