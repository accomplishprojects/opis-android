package com.android.opisapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.widget.TextView;

import com.android.opisapp.R;
import com.android.opisapp.utilities.animation.Titanic;
import com.android.opisapp.utilities.animation.TitanicTextView;


/**
 * Created by luigigo on 8/30/15.
 */
public class SplashScreen extends Activity {

    private Titanic titanicAnimation;
    private TitanicTextView splashText1;
    private TextView splashText2;
    private Handler mTimer;
    private Intent i;
    private Typeface font;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mTimer = new Handler();
        font = Typeface.createFromAsset(getAssets(),
                "lato-black.ttf");
        splashText1 = (TitanicTextView) findViewById(R.id.splash_text1);
        splashText1.setTypeface(font);
        splashText2 = (TextView) findViewById(R.id.splash_text2);
        splashText2.setTypeface(font);

        titanicAnimation = new Titanic();
        titanicAnimation.start(splashText1);

        mTimer.postDelayed(new Runnable() {
            @Override
            public void run() {
                i = new Intent(SplashScreen.this, LoginActivity.class);
                startActivity(i);
                titanicAnimation.cancel();
                finish();
            }
        }, 10500);
    }
}
