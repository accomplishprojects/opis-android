package com.android.opisapp.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.opisapp.AppController;
import com.android.opisapp.R;
import com.android.opisapp.model.UserModel;
import com.android.opisapp.utilities.Constants;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by luigigo on 9/5/15.
 */
public class Fragment_Reset_Password extends Fragment implements View.OnClickListener {

    static ProgressDialog mProgressDialog;
    private static UserModel userModel = new UserModel();
    String responseBody, responseResult, password, confirmpassword;
    private Button btnResetPassword;
    private HashMap<String, String> params;
    private EditText edtPassword, edtConfirmPassword;


    public static void showDialog(Activity s) {
        mProgressDialog = new ProgressDialog(s);
        // Set progressdialog title
        mProgressDialog.setTitle("Loading");
        // Set progressdialog message
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setIndeterminate(false);
        // Show progressdialog
        mProgressDialog.show();
    }

    public static void hideDialog() {
        // Close the progressdialog
        if (mProgressDialog != null)
            mProgressDialog.dismiss();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reset_password, container, false);

        edtPassword = (EditText) view.findViewById(R.id.edtPassword);
        edtConfirmPassword = (EditText) view.findViewById(R.id.edtConfirmPassword);
        btnResetPassword = (Button) view.findViewById(R.id.btnResetPassword);
        btnResetPassword.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnResetPassword:

                password = edtPassword.getText().toString();
                confirmpassword = edtConfirmPassword.getText().toString();

                if (password.isEmpty() || confirmpassword.isEmpty()) {
                    Toast.makeText(getActivity(), "Please check all blank fields", Toast.LENGTH_SHORT).show();
                } else if (!confirmpassword.equals(password)) {
                    Toast.makeText(getActivity(), "Password not match", Toast.LENGTH_SHORT).show();
                } else {
                    params = new HashMap<String, String>();
                    params.put("password", confirmpassword);

                    String url = Constants.baseURL + Constants.resetPassword;
                    resetPassword(url);
                    showDialog(getActivity());
                }
                break;

        }
    }


    private void resetPassword(String url) {

        // Tag used to cancel the request
        String tag_json_obj = "json_obj_req";

        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("JSON-Response", response.toString());

                        try {
                            responseBody = response.toString();
                            if (responseBody.equals("{\"response\":\"Password successfully reset\"}")) {
                                responseResult = response.getString("response");
                                hideDialog();
                                Toast.makeText(getActivity(), responseResult, Toast.LENGTH_SHORT).show();

                                edtPassword.setText("");
                                edtConfirmPassword.setText("");
                            } else {
                                responseResult = response.getString("error");
                                hideDialog();
                                Toast.makeText(getActivity(), responseResult, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error-Volley", "Error: " + error);
                Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Accept", "application/json");
                headers.put("token", userModel.getToken());
                return headers;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }

}
