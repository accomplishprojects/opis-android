package com.android.opisapp.utilities;

/**
 * Created by luigigo on 9/21/15.
 */
public class Constants {

    public static String baseURL = "http://sandovalvictorianoob-gynclinic.com/api/";
    public static String registrationURL = "registration.php";
    public static String loginURL = "user.php";
    public static String logoutURL = "logout.php";
    public static String appointmentURL = "appointment.php";
    public static String appointmentHistory = "history.php";
    public static String cancelAppointment = "cancelAppointment.php";
    public static String doctorStatus = "doctorStatus.php";
    public static String reschedAppointment = "updateAppointment.php";
    public static String sendMessage = "message.php";
    public static String resetPassword = "resetPassword.php";

    public static String googleMapAPIKey = "AIzaSyBBfPjRSEtf5MfsujfXRV5fuwt8HG5c9aU";
}
